//
//  ViewController.swift
//  SuperGuincho
//
//  Created by Fabiola Werneck on 05/06/15.
//  Copyright (c) 2015 SuperGuincho. All rights reserved.
//

import UIKit
import MapKit

class HomeViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet var menuButton: UIBarButtonItem!
    @IBOutlet var map: MKMapView!
    @IBOutlet var addressLabel: UILabel!
    
    //location manager to authorize user location for Maps app
    var manager:CLLocationManager!
    
    let regionRadius: CLLocationDistance = 1000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setup our Location Manager
        if (CLLocationManager.locationServicesEnabled())
        {
            manager = CLLocationManager()
            manager.delegate = self
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.requestAlwaysAuthorization()
            manager.startUpdatingLocation()
        }
        
        //Setup our Map View
        map.delegate = self
        map.mapType = MKMapType.Standard
        map.showsUserLocation = true
    }
    
    func locationManager(manager:CLLocationManager, didUpdateLocations locations:[AnyObject]) {
        var currentLocation = MKCoordinateRegion(center: map.userLocation.coordinate, span: MKCoordinateSpanMake(0.007, 0.007))
        map.setRegion(currentLocation, animated: false)
        
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error)->Void in
            if (error != nil) {
                println("Reverse geocoder failed with error" + error.localizedDescription)
                return
            }
            
            if placemarks.count > 0 {
                let pm = placemarks[0] as! CLPlacemark
                self.displayLocationInfo(pm)
            } else {
                println("Problem with the data received from geocoder")
            }
        })
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        //stop updating location to save battery life
        //manager.stopUpdatingLocation()
        addressLabel.text = placemark.addressDictionary["Street"] as? String
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

