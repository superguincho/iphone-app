//
//  ViewController.swift
//  SuperGuincho
//
//  Created by Fabiola Werneck on 05/06/15.
//  Copyright (c) 2015 SuperGuincho. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    var homeVC : HomeViewController!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //manager userdata
        
        let userData = NSUserDefaults.standardUserDefaults()
        
        userData.setValue("fabiwerneck", forKey: User.username)
        userData.synchronize()
        
        //get
        //let defaults = NSUserDefaults.standardUserDefaults()
        
        if let username = userData.valueForKey(User.username) as? String {
            showHomeView()
        }
    }
    
    func showHomeView()
    {
        homeVC = storyboard?.instantiateViewControllerWithIdentifier("homeVC") as! HomeViewController;
        
        homeVC.view.alpha = 0.0;
        
        self.addChildViewController(homeVC);
        self.view.addSubview(homeVC.view);
        
        UIView.animateWithDuration(0.4, animations: { () -> Void in
            self.homeVC.view.alpha = 1.0;
            }) { (Bool) -> Void in
                self.homeVC.didMoveToParentViewController(self);
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

