//
//  VehicleDataModel.swift
//  SuperGuincho
//
//  Created by Fabiola Werneck on 04/09/15.
//  Copyright (c) 2015 SuperGuincho. All rights reserved.
//


import Foundation

class Vehicle: NSObject, Printable {
    let name: String
    let licensePlate: String
    
    override var description: String {
        return "Name: \(name), License Plate: \(licensePlate)\n"
    }
    
    init(item: JSON) {
        self.name = item["title"].string ?? ""
        self.licensePlate = item["body"].string ?? ""
    }
    
    init(name: String?, licensePlate: String?) {
        self.name = name ?? ""
        self.licensePlate = licensePlate ?? ""
    }
    
    //MARK: Endpoints
    
    class func endpointForVehicles() -> String {
        return "http://jsonplaceholder.typicode.com/posts"
    }
    
    //MARK: getters
    
    class func getVehicles(completionHandler: ((vehiclesArray: [Vehicle]?, error: NSError?)->Void)?) -> Void {
        
        var vehicles = [Vehicle]()
        
        request(.GET, Vehicle.endpointForVehicles())
            .responseJSON { (request, response, data, error) in
                if let anError = error
                {
                    completionHandler?(vehiclesArray: nil, error: error)
                }
                else if let data: AnyObject = data
                {
                    // handle the results as JSON, without a bunch of nested if loops
                    let json = JSON(data)
                    
                    if let vehiclesResponse = json.array {
                        
                        for item in vehiclesResponse {
                            var vehicle = Vehicle(item: item)
                            vehicles.append(vehicle)
                        }
                    }
                    
                    completionHandler?(vehiclesArray: vehicles, error: nil)
                }
                
            }.responseString{ (request, response, stringResponse, error) in
                // print response as string for debugging, testing, etc.
                //println(stringResponse)
        }
    }
}