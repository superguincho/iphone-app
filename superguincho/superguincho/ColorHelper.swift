//
//  ColorHelper.swift
//  SuperGuincho
//
//  Created by Fabiola Werneck on 09/08/15.
//  Copyright (c) 2015 SuperGuincho. All rights reserved.
//

import Foundation
import UIKit

func uiColorFromHex(rgbValue:UInt32) -> UIColor {
    
    let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
    let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
    let blue = CGFloat(rgbValue & 0xFF)/256.0
    
    return UIColor(red:red, green:green, blue:blue, alpha:1.0)
}
