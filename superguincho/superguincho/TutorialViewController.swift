//
//  ViewController.swift
//  SuperGuincho
//
//  Created by Fabiola Werneck on 05/06/15.
//  Copyright (c) 2015 SuperGuincho. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController, UIScrollViewDelegate {
    
    //@IBOutlet var scrollView: UIScrollView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var whiteView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.scrollView.contentSize = CGSizeMake(self.whiteView.frame.width * 2, self.whiteView.frame.height)
        //self.scrollView.delegate = self
        //self.pageControl.currentPage = 0
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        var pageWidth:CGFloat = CGRectGetWidth(scrollView.frame)
        var currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        
        // Change the text accordingly
        if Int(currentPage) == 0{
            textView.text = "Chegamos para facilitar a sua vida na hora do aperto.\n\nNosso trabalho é encurtar a distância entre você e a assistência veicular móvel mais próxima."
        }else{
            textView.text = "Na sua primeira utilização será necessário fazer um rápido cadastro.\n\nMas fique tranquilo!\nSeus dados estão seguros. Se tiver dúvidas consulte nossa política de privacidade."
            // Show the "Let's Start" button in the last slide (with a fade in animation)
            //UIView.animateWithDuration(1.0, animations: { () -> Void in
            //    self.startButton.alpha = 1.0
            //})
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}